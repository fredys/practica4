<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/tabs.css">
    <link rel="stylesheet" href="css/delimeters.css">
    <title>delimitadores de Php</title>
  </head>
  <body>
    <header>
      <h1>Los delimitadores de codigo PHP</h1>
    </header>
    <section>
      <article class="">
        <div class="contenedor-tabs">
          <?php
            echo "<span class=\"diana\" id=\"una\"></span>\n";
            echo "<div class=\"tab\">";
            echo "<a href=\"#una\" class=\"tab-e\">Estilo XML</a>\n";
            echo "<div class=\"first\">\n";
            echo "<p class=\"xmltag\"\n>";
            echo "Este texto esta escrito en Php utilizando las etiquetas mas";
            echo "usuales y recomendadas para delimitar el codigo PHP que son";
            echo "&lt; ?php... ? &gt; . <br> \n";
            echo "</p>\n";
            echo "</div>\n";
            echo "</div>\n";
           ?>

           <?
             echo "<span class=\"diana\" id=\"tres\"></span>\n";
             echo "<div class=\"tab\">";
             echo "<a href=\"#tres\" class=\"tab-e\">Etiquetas cortas</a>\n";
             echo "<div>\n";
             echo "<p class=\"shorttag\"\n>";
             echo "Este texto esta escrito en Php utilizando las etiquetas ";
             echo "cortas <br> \n que son: &lt; ?... ? &gt; .  \n";

             echo "</p>\n";
             echo "</div>\n";
             echo "</div>\n";
            ?>
        </div>

      </article>
    </section>


  </body>
</html>
